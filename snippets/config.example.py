# coding=utf-8
from sqlalchemy import MetaData

db_info = {
    'username': '',
    'password': '',
    'host': '',
    'port': 3306,
    'db_name': ''
}

metadata = MetaData(
    naming_convention={
        'pk': 'pk_%(table_name)s',
        'fk': 'fk_%(table_name)s_%(referred_table_name)s_%(referred_column_0_name)s',
        'uq': 'uq_%(table_name)s_%(column_0_name)s',
        'ix': 'ix_%(column_0_label)s',
        'ck': 'ck_%(table_name)s_%(constraint_name)s'
    }
)

SECRET_KEY = ''  # Put an os.urandom(32) in here
WTF_SECRET_KEY = ''  # Put an os.urandom(32) in here

# Sample:
# SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{0[username]}:{0[password]}@{0[host]}:{0[port]}/{0[db_name]}?charset=utf8'\
#     .format(db_info)
SQLALCHEMY_DATABASE_URI = ''
SQLALCHEMY_POOL_RECYCLE = 3600
SQLALCHEMY_POOL_TIMEOUT = 20
SQLALCHEMY_TRACK_MODIFICATIONS = False