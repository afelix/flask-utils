# encoding=utf-8
from wtforms import TextAreaField, BooleanField, StringField, PasswordField, SelectField, IntegerField, \
    SelectMultipleField, ValidationError
from wtforms.widgets import TextArea

from sqlalchemy.exc import IntegrityError

# Helpers
BS3_ATTRIBUTES = ['help_text']


class BS3Helper(object):
    def __init__(self, help_text=None):
        self.help_text = help_text


# Fields
class BS3TextAreaField(TextAreaField):
    widget = TextArea()

    def __init__(self, **kwargs):
        bs3_kwargs = dict()
        for attr in BS3_ATTRIBUTES:
            if attr in kwargs:
                bs3_kwargs[attr] = kwargs[attr]
                kwargs.pop(attr)

        super(BS3TextAreaField, self).__init__(**kwargs)

        self.bs3 = BS3Helper(**bs3_kwargs)


class CKTextAreaWidget(TextArea):
    def __call__(self, field, **kwargs):
        kwargs.setdefault('class_', 'ckeditor')
        return super(CKTextAreaWidget, self).__call__(field, **kwargs)


class CKTextAreaField(BS3TextAreaField):
    widget = CKTextAreaWidget()


class BS3StringField(StringField):
    def __init__(self, **kwargs):
        bs3_kwargs = dict()
        for attr in BS3_ATTRIBUTES:
            if attr in kwargs:
                bs3_kwargs[attr] = kwargs[attr]
                kwargs.pop(attr)

        super(BS3StringField, self).__init__(**kwargs)

        self.bs3 = BS3Helper(**bs3_kwargs)


class BS3SelectField(SelectField):
    def __init__(self, **kwargs):
        bs3_kwargs = dict()
        for attr in BS3_ATTRIBUTES:
            if attr in kwargs:
                bs3_kwargs[attr] = kwargs[attr]
                kwargs.pop(attr)

        super(BS3SelectField, self).__init__(**kwargs)

        self.bs3 = BS3Helper(**bs3_kwargs)


class BS3PasswordField(PasswordField):
    def __init__(self, **kwargs):
        bs3_kwargs = dict()
        for attr in BS3_ATTRIBUTES:
            if attr in kwargs:
                bs3_kwargs[attr] = kwargs[attr]
                kwargs.pop(attr)

        super(BS3PasswordField, self).__init__(**kwargs)

        self.bs3 = BS3Helper(**bs3_kwargs)


class BS3BooleanField(BooleanField):
    def __init__(self, **kwargs):
        bs3_kwargs = dict()
        for attr in BS3_ATTRIBUTES:
            if attr in kwargs:
                bs3_kwargs[attr] = kwargs[attr]
                kwargs.pop(attr)

        super(BS3BooleanField, self).__init__(**kwargs)

        self.bs3 = BS3Helper(**bs3_kwargs)


class BS3IntegerField(IntegerField):
    def __init__(self, **kwargs):
        bs3_kwargs = dict()
        for attr in BS3_ATTRIBUTES:
            if attr in kwargs:
                bs3_kwargs[attr] = kwargs[attr]
                kwargs.pop(attr)

        super(BS3IntegerField, self).__init__(**kwargs)

        self.bs3 = BS3Helper(**bs3_kwargs)


class BS3SelectMultipleField(SelectMultipleField):
    def __init__(self, **kwargs):
        bs3_kwargs = dict()
        for attr in BS3_ATTRIBUTES:
            if attr in kwargs:
                bs3_kwargs[attr] = kwargs[attr]
                kwargs.pop(attr)

        super(BS3SelectMultipleField, self).__init__(**kwargs)

        self.bs3 = BS3Helper(**bs3_kwargs)


class Select2Field(BS3SelectField):
    model = None
    db = None
    error_msg = 'De optie {0!r} kan niet worden geselecteerd'

    def pre_validate(self, form):
        if self.data not in self.choices:
            if not self.model:
                # New options are not allowed
                raise ValidationError(self.error_msg.format(self.data))
            option = self.model(self.data)
            self.db.session.add(option)
            try:
                self.db.session.commit()
            except IntegrityError:
                raise ValidationError(self.error_msg.format(self.data))


class Select2MultipleField(BS3SelectMultipleField):
    model = None
    db = None
    error_msg = 'De optie {0!r} kan niet worden geselecteerd'

    def pre_validate(self, form):
        if self.data:
            values = list(c[0] for c in self.choices)
            for d in self.data:
                if d not in values:
                    if not self.model:
                        # New options are not allowed
                        raise ValidationError(self.error_msg.format(d))
                    try:
                        int(d)
                    except ValueError:
                        # New option
                        option = self.model(d)
                        self.db.session.add(option)
                        try:
                            self.db.session.commit()
                        except IntegrityError:
                            raise ValidationError(self.error_msg.format(d))
